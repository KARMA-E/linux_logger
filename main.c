#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include <sys/stat.h>

#include <fcntl.h>
#include <unistd.h>
#include <termios.h>

#include <time.h>
#include <signal.h>

#define OPEN_ERROR          (-1)

#define BUFFER_SIZE         (10 * 1024 * 1024)
#define LOG_DIR_NAME        "logStash"

#define LOGGER_NAMES_SIZE   (50)
#define LOGGER_OFFSET_EN    (0)
#define LOGGER_STDOUT_EN    (1)

typedef struct 
{
    int  Port;
    char PortName[LOGGER_NAMES_SIZE];
    int  File;
    char FileName[LOGGER_NAMES_SIZE];
} logger_s;


static logger_s _logger = {.Port = OPEN_ERROR, .File = OPEN_ERROR};


void _GoOut(int par)
{
    printf("\n");
    
    if (_logger.File != OPEN_ERROR)
    {
        printf("Close file \"%s\"\n", _logger.FileName);
        close(_logger.File);
    }

    if (_logger.Port != OPEN_ERROR)
    {
        printf("Close port \"%s\"\n", _logger.PortName);
        tcflush(_logger.Port, TCIFLUSH);
        close(_logger.Port);
    }

    exit(1);
}


static char * _GetTime(void)
{   
    static char strTime[20];
    static uint32_t clockOffset = 0;

    if (clockOffset == 0)
    {
        time_t curTime = time(NULL);
        while(curTime == time(NULL)){}
        clockOffset = clock();
    }
    
    time_t sysTime = time(NULL);
    uint32_t clk = clock() - clockOffset;

    struct tm * curTime = localtime(&sysTime);
    uint16_t len = strftime(strTime, sizeof(strTime), "%H:%M:%S", curTime);
    sprintf(strTime + len, ".%03lu", (clk % CLOCKS_PER_SEC) / 1000);

    return strTime;
}


static char * _GetDate(void)
{   
    static char strDate[20];
    
    time_t sysTime = time(NULL);
    struct tm * curTime = localtime(&sysTime);
    strftime(strDate, sizeof(strDate), "%Y-%m-%d", curTime);

    return strDate;
}


static void _OpenNewLogger(char * portName, logger_s * logger)
{
    struct stat tempStat;

    if (stat(LOG_DIR_NAME, &tempStat) == OPEN_ERROR)
    {
        mkdir(LOG_DIR_NAME, S_IRUSR | S_IWUSR | S_IXUSR);
        printf("\033[33mCreate logger dir \"%s\"\033[37m\n", LOG_DIR_NAME);
    }

    sprintf(logger->PortName, "/dev/%s", portName);
    logger->Port = open(logger->PortName, O_RDWR | O_NOCTTY);

    if (logger->Port != OPEN_ERROR)
    {
        printf("Open port \"%s\" (hdr %d)\n", logger->PortName, logger->Port);
        uint32_t fileInd = 0;
        
        do
        {
            sprintf(logger->FileName, LOG_DIR_NAME "/log_%s_%s_%u.txt", _GetDate(), portName, fileInd);
            logger->File = open(logger->FileName, O_RDONLY);

            if (logger->File != OPEN_ERROR)
            {
                close(logger->File);
                fileInd++;
            }
        
        } while (logger->File != OPEN_ERROR);

        logger->File = open(logger->FileName, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
    
        printf("Open file \"%s\" - %s (hdr %d)\n\n", logger->FileName, (logger->File != OPEN_ERROR) ? "OK" : "FAIL", logger->File);
    }
    else
    {
        printf("\033[31mPort \"%s\" doesn't exist\033[37m\n", logger->PortName);
    }
}

//----------------------------------------------------------------------------------------------//

int main(int argc, char * argv[])
{
    system("clear");
    setvbuf(stdout , NULL, _IONBF, 0);
    signal(SIGINT, _GoOut);

    uint16_t strOffset = 0;

    if (argc <= 1)
    {
        _OpenNewLogger("ttyUSB0", &_logger);
    }
    else
    {
        _OpenNewLogger(argv[1], &_logger);
    }

    if (argc > 2)
    {
        strOffset = atoi(argv[2]);
        printf("Set str offset %u\n", strOffset);
    }

    if (_logger.Port != OPEN_ERROR && _logger.File != OPEN_ERROR)
    {
        //system("stty 921600 cs8 -parenb cstopb -F /dev/ttyUSB0");

        struct termios portSettings;
        ////tcgetattr(newLogger.Port, &portSettings);

        cfsetispeed(&portSettings, B921600);
        cfsetospeed(&portSettings, B921600);

        portSettings.c_cflag &= ~PARENB;
        portSettings.c_cflag |= CSTOPB;
        portSettings.c_cflag &= ~CSIZE;
        portSettings.c_cflag |= CS8;
        portSettings.c_cflag &= ~CRTSCTS;
        portSettings.c_cflag |= CREAD | CLOCAL;
        ////sdiportSettings.c_iflag &= ~(IXON | IXOFF | IXANY);
        ////portSettings.c_iflag &= ~(ICANON | ECHO | ECHOE | ISIG);
        portSettings.c_oflag &= ~OPOST;

        portSettings.c_cc[VMIN] = 1;
        portSettings.c_cc[VTIME] = 100;

        tcsetattr(_logger.Port, TCSANOW, &portSettings);
        fcntl(_logger.Port, F_SETFL, FNDELAY);

        static char rxBuf[BUFFER_SIZE] = {0};
        static char dataBuf[BUFFER_SIZE] = {0};

        bool recNewDataFlag = false;
        
        while (true)
        {
            uint32_t dataCnt = 0;
            int32_t rxCnt = 0;
            
            while ((rxCnt = read(_logger.Port, rxBuf, 1)) == -1){}

            while ((rxCnt != -1) && (dataCnt < (3 * sizeof(dataBuf)) / 4))
            {   
                recNewDataFlag = true;
                
                if (rxCnt > 10)
                {
                    printf("\033[33m | WRN! may overflow local buffer %u | \033[37m\n", rxCnt);
                }

                for (uint16_t i = 0; i < rxCnt; i++)
                {
                    if (rxBuf[i] == '\n')
                    {
                        dataCnt += sprintf(dataBuf + dataCnt, "\n%s",  _GetDate());
                        dataCnt += sprintf(dataBuf + dataCnt, " %s  ", _GetTime());

#if LOGGER_OFFSET_EN
                        for (uint16_t mv = 0; mv < strOffset; mv++)
                        {
                            dataBuf[dataCnt] = '\t';
                            dataCnt++;
                        }
                        dataCnt += sprintf(dataBuf + dataCnt, " | ");
#endif                      
                    }

                    if (rxBuf[i] != '\r' && rxBuf[i] != '\n')
                    {
                        dataBuf[dataCnt] = rxBuf[i];
                        dataCnt++;
                    }
                }   

                uint32_t wait = 0xFFF;
                while (((rxCnt = read(_logger.Port, rxBuf, 1)) == -1) && (--wait != 0)) {}
            }

            if (recNewDataFlag)
            {
                recNewDataFlag = false;

                dataBuf[dataCnt] = 0;

#if LOGGER_STDOUT_EN
                printf("%s", dataBuf);
#endif
                //uint32_t clkDiff = clock();
                write(_logger.File, dataBuf, dataCnt);
                //clkDiff = clock() - clkDiff;
                //printf("\033[33m |Save time %u us| \033[37m", clkDiff);
            }
        }
    }
    else
    {
        printf("\033[31mLogger start fail!\033[33m (change port)\033[37m\n");
    }

    _GoOut(1);

    return 0;
}


